CC = gcc
CFLAGS = -std=c99 -Wall
TESTFLAGS = -fprofile-arcs -ftest-coverage

app: jeu.h jeu.c app.c
    # build the app
    $(CC) $(CFLAGS) -o app app.c jeu.c

    # run the app
    ./app

testcomparaison: testcomparaison.c jeu.h jeu.c
    # build the comparaison test
    $(CC) $(CFLAGS) $(TESTFLAGS) -o testcomparaison testcomparaison.c jeu.c

    # run the test, which will generate testcomparaison.gcna and testcomparaison.gcno
    ./testcomparaison

    # compute how test is covering testcomparaison.c
    gcov -c -p testcomparaison.c

testhasard: testhasard.c jeu.h jeu.c
    # build the hasard test
    $(CC) $(CFLAGS) $(TESTFLAGS) -o testhasard testhasard.c jeu.c

    # run the hasard test, which will generate testhasard.gcna and testhasard.gcno
    ./testhasard

    # compute how test is covering testhasard.c
    gcov -c -p testhasard.c

testintegration: testintegration.c jeu.h jeu.c
    # build the integration test
    $(CC) $(CFLAGS) $(TESTFLAGS) -o testintegration testintegration.c jeu.c

    # run the integration test
    ./testintegration P R R P C C P R

    # compute how test is covering testintegration.c
    gcov -c -p testintegration.c

clean:
    rm -f *.o app testintegration testhasard testcomparaison app *.gcov *.gcda *.gcno
